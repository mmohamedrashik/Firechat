package guy.droid.im.firechat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mFirebaseDatabaseReference;
    public static final String MESSAGES_CHILD = "messages";
    public static final String REGISTERED_USERS = "users";
    private FirebaseRecyclerAdapter<ChatContents, ChatViewHolder> mFirebaseAdapter;

    String mUsername,mPhotoUrl,mEmailid,mUniqueId;
    TextView user_;
    EditText message;
    Button button;
    RecyclerView mRecyclerview;
    LinearLayoutManager mLinearLayoutManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        message = (EditText)findViewById(R.id.message);
        button  = (Button)findViewById(R.id.send);
        user_ = (TextView)findViewById(R.id.user_name);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        mRecyclerview = (RecyclerView)findViewById(R.id.chat_recycler);
        mRecyclerview.setLayoutManager(mLinearLayoutManager);
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            mEmailid = mFirebaseUser.getEmail();
            mUniqueId = mFirebaseUser.getUid();

            user_.setText(mUsername);
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
                Log.e("URL_",mPhotoUrl);
            }
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send_message();
            }
        });
        mFirebaseDatabaseReference.child(REGISTERED_USERS).orderByChild("id").equalTo(mUniqueId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot!=null && dataSnapshot.getChildren()!=null &&
                        dataSnapshot.getChildren().iterator().hasNext()){
                    //Username Exist
                    Toast.makeText(MainActivity.this, "YES", Toast.LENGTH_SHORT).show();
                }else {
                    //Username Does Not Exist
                    Toast.makeText(MainActivity.this, "NOT", Toast.LENGTH_SHORT).show();
                    ChatContents chats = new ChatContents();
                    chats.setId(mUniqueId);
                    chats.setEmail_id(mEmailid);
                    chats.setImage_url(mPhotoUrl);
                    chats.setName(mUsername);
                    mFirebaseDatabaseReference.child(REGISTERED_USERS).push().setValue(chats);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mFirebaseAdapter = new FirebaseRecyclerAdapter<ChatContents, ChatViewHolder>(
                ChatContents.class,
                R.layout.chat_single,
                ChatViewHolder.class,
                mFirebaseDatabaseReference.child(MESSAGES_CHILD)
        ) {
            @Override
            protected void populateViewHolder(ChatViewHolder viewHolder, ChatContents model, int position) {
                viewHolder.chatText.setText(model.email_id+"-"+model.getMessage());
            }
        };
        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                int lastVisiblePosition =
                        mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the
                // user is at the bottom of the list, scroll to the bottom
                // of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mRecyclerview.scrollToPosition(positionStart);
                }
            }
        });
        mRecyclerview.setAdapter(mFirebaseAdapter);
    }
    public void send_message()
    {
        String mMessage = message.getText().toString();
        ChatContents chatContents = new ChatContents();
        chatContents.setId(mUniqueId);
        chatContents.setName(mUsername);
        chatContents.setImage_url(mPhotoUrl);
        chatContents.setEmail_id(mEmailid);
        chatContents.setMessage(mMessage);
        mFirebaseDatabaseReference.child(MESSAGES_CHILD)
                .push().setValue(chatContents);
    }
    public static class ChatViewHolder extends RecyclerView.ViewHolder {
        TextView chatText;
        public ChatViewHolder(View v) {
            super(v);
            chatText = (TextView) itemView.findViewById(R.id.s_chat_message);
        }
    }

    public void ChatUsers(View view)
    {
        startActivity(new Intent(getApplicationContext(),ChatUsers.class));
    }
}
