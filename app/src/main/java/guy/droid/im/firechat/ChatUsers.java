package guy.droid.im.firechat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatUsers extends AppCompatActivity {
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mFirebaseDatabaseReference;
    private static final String CHATS_USER = "chats_user";
    public static final String MESSAGES_CHILD = "messages";
    public static final String REGISTERED_USERS = "users";
    ArrayList<ChatContents> current_users;
    public RecyclerView mUsers;
    ChatUserAdapter chatuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_users);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        current_users = new ArrayList<>();

        mUsers = (RecyclerView)findViewById(R.id.recycler_user_chat);
        LinearLayoutManager liner = new LinearLayoutManager(this);
        mUsers.setLayoutManager(liner);

        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        } else {
           Log.e(CHATS_USER,mFirebaseUser.getDisplayName());
        }

        mFirebaseDatabaseReference.child(REGISTERED_USERS).orderByChild("id").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> objectMap = (HashMap<String,Object>) dataSnapshot.getValue();
                current_users.clear();
                for (Object obj : objectMap.values()) {
                    if (obj instanceof Map) {
                        Map<String, Object> mapObj = (Map<String, Object>) obj;
                        ChatContents chat_content = new ChatContents();
                        chat_content.setId((String)mapObj.get("id"));
                        chat_content.setName((String)mapObj.get("name"));
                        chat_content.setImage_url((String)mapObj.get("image_url"));
                        chat_content.setEmail_id((String)mapObj.get("email_id"));
                        current_users.add(chat_content);
                    }
                }

                for(int i=0;i<current_users.size();i++)
                {
                    try {
                        Log.d(CHATS_USER,current_users.get(i).getEmail_id());
                }catch (Exception e)
                {

                }
                }

                chatuser.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        chatuser = new ChatUserAdapter(ChatUsers.this,current_users);
        mUsers.setAdapter(chatuser);
    }

    public void UserSelected(ChatContents chat)
    {
        Intent intent = new Intent(getApplicationContext(),PrivateChat.class);
        intent.putExtra("reciever_id",chat.getId());
        startActivity(intent);
      //  Log.d("GENERATED",chat.getName());
    }

}
