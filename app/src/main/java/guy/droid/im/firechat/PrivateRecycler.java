package guy.droid.im.firechat;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 7/25/2017.
 */

public class PrivateRecycler extends RecyclerView.Adapter<PrivateRecycler.ViewHolder> {

    PrivateChat privateChat;
    ArrayList<ChatMessage> chatMessage;
    String mUniqueid;
    public PrivateRecycler(PrivateChat privateChat, ArrayList<ChatMessage> chatMessage,String mUniqueid)
    {
        this.privateChat = privateChat;
        this.chatMessage = chatMessage;
        this.mUniqueid = mUniqueid;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PrivateRecycler.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.private_chat,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


            if(mUniqueid.equals(chatMessage.get(position).getSender_id()))
            {
                holder.sender.setVisibility(View.VISIBLE);
                holder.receiver.setVisibility(View.INVISIBLE);
                holder.sender.setText(chatMessage.get(position).getMessage());

            }else
            {
                holder.receiver.setVisibility(View.VISIBLE);
                holder.sender.setVisibility(View.INVISIBLE);
                holder.receiver.setText(chatMessage.get(position).getMessage());
            }
    }

    @Override
    public int getItemCount() {
        return chatMessage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView sender,receiver;

        public ViewHolder(View itemView) {
            super(itemView);

            sender  =  (TextView) itemView.findViewById(R.id.sender_message);
            receiver = (TextView) itemView.findViewById(R.id.reciever_message);

        }
    }
}
