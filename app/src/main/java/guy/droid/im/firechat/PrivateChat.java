package guy.droid.im.firechat;

import android.content.Intent;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrivateChat extends AppCompatActivity {
    String reciever_id;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mFirebaseDatabaseReference;
    public static final String MESSAGES_CHILD = "messages";
    public static final String REGISTERED_USERS = "users";
    public static final String CHAT_ROOM = "chat";
    Button send;
    EditText message_box;
    RecyclerView mRecyclerView;
    String mUsername,mPhotoUrl,mEmailid,mUniqueId;
    ArrayList<ChatMessage> chats;
    public static final String CHATS_USER = "private_chats";
    PrivateRecycler privaterecycler;
    private FirebaseRecyclerAdapter<ChatMessage, PrivateChat.ChatViewHolder> mFirebaseAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_chat);

        chats =new ArrayList<>();
        Bundle extras = getIntent().getExtras();
        reciever_id = extras.getString("reciever_id");
        send = (Button)findViewById(R.id.send_button);
        message_box = (EditText)findViewById(R.id.send_text);
        mRecyclerView = (RecyclerView)findViewById(R.id.p_chat_recycler);
        final LinearLayoutManager linear = new LinearLayoutManager(this);
        linear.setStackFromEnd(true);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            mEmailid = mFirebaseUser.getEmail();
            mUniqueId = mFirebaseUser.getUid();

            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
                Log.e("URL_",mPhotoUrl);
            }
        }

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message_ChatSend();
            }
        });
        privaterecycler = new PrivateRecycler(PrivateChat.this,chats,mUniqueId);
        mRecyclerView.setLayoutManager(linear);
        mRecyclerView.setAdapter(privaterecycler);

       /* Query queryRef = mFirebaseDatabaseReference.child(CHAT_ROOM).child(reciever_id+""+mUniqueId);
        queryRef.orderByChild("timestamp");
        queryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        Log.e("WARN",dataSnapshot.toString());
                        Map<String, Object> objectMap = (HashMap<String, Object>) dataSnapshot.getValue();

                        chats.clear();

                        for (Object obj : objectMap.values()) {
                            if (obj instanceof Map) {
                                Map<String, Object> mapObj = (Map<String, Object>) obj;
                                ChatMessage chat_content = new ChatMessage();
                                chat_content.setId((String) mapObj.get("id"));
                                chat_content.setSender_id((String) mapObj.get("sender_id"));
                                chat_content.setReciever_id((String) mapObj.get("reciever_id"));
                                chat_content.setMessage((String) mapObj.get("message"));
                                chats.add(chat_content);
                            }
                        }

                        for (int i = 0; i < chats.size(); i++) {
                            try {
                                Log.d(CHATS_USER, chats.get(i).getMessage());
                            } catch (Exception e) {

                            }
                        }
                    }catch(Exception e)
                    {

                    }
                     Collections.reverse(chats);
                    privaterecycler.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        }); */
        mFirebaseAdapter = new FirebaseRecyclerAdapter<ChatMessage, ChatViewHolder>(
                ChatMessage.class,
                R.layout.private_chat,
                ChatViewHolder.class,
                mFirebaseDatabaseReference.child(CHAT_ROOM).child(reciever_id+""+mUniqueId)
        ) {
            @Override
            protected void populateViewHolder(ChatViewHolder holder, ChatMessage model, int position) {
             //   viewHolder.chatText.setText(model.email_id+"-"+model.getMessage());
                holder.receiver.setVisibility(View.INVISIBLE);
                holder.sender.setVisibility(View.INVISIBLE);
                if(mUniqueId.equals(model.getSender_id()))
                {
                    holder.sender.setVisibility(View.VISIBLE);
                    holder.receiver.setVisibility(View.INVISIBLE);
                    holder.sender.setText(model.getMessage());

                }else
                {
                    holder.receiver.setVisibility(View.VISIBLE);
                    holder.sender.setVisibility(View.INVISIBLE);
                    holder.receiver.setText(model.getMessage());
                }
            }
        };
        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                int lastVisiblePosition =
                        linear.findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the
                // user is at the bottom of the list, scroll to the bottom
                // of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mRecyclerView.scrollToPosition(positionStart);
                }
            }
        });
        mRecyclerView.setAdapter(mFirebaseAdapter);
    }

    public static class ChatViewHolder extends RecyclerView.ViewHolder {
        TextView sender,receiver;

        public ChatViewHolder(View itemView) {
            super(itemView);

            sender  =  (TextView) itemView.findViewById(R.id.sender_message);
            receiver = (TextView) itemView.findViewById(R.id.reciever_message);

        }
    }
    public void Message_ChatSend()
    {
        String message = message_box.getText().toString();
        ChatMessage chatmessage = new ChatMessage();
        chatmessage.setId(mUniqueId);
        chatmessage.setSender_id(mUniqueId);
        chatmessage.setReciever_id(reciever_id);
        chatmessage.setTimestamp(System.currentTimeMillis());
        chatmessage.setMessage(message);
        String user_1 = mUniqueId+""+reciever_id;
        String user_2 = reciever_id+""+mUniqueId;
        mFirebaseDatabaseReference.child(CHAT_ROOM).child(user_1)
                .push().setValue(chatmessage);
        mFirebaseDatabaseReference.child(CHAT_ROOM).child(user_2)
                .push().setValue(chatmessage);
    }

}
