package guy.droid.im.firechat;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 7/24/2017.
 */

public class ChatUserAdapter extends RecyclerView.Adapter<ChatUserAdapter.ViewHolder> {
    ChatUsers chatUsers;
    ArrayList<ChatContents> chatcontent;
    View view;
    public ChatUserAdapter(ChatUsers chatUsers, ArrayList<ChatContents> chatcontent)
    {
        this.chatUsers = chatUsers;
        this.chatcontent = chatcontent;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_single_users,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

            holder.name.setText(chatcontent.get(position).getName());
            holder.email.setText(chatcontent.get(position).getEmail_id());
            holder.chat_view.setOnClickListener(Choose(position,holder));

    }

    public View.OnClickListener Choose(final int position, final ViewHolder holder)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showEditDialog(position, holder);
                chatUsers.UserSelected(chatcontent.get(position));
            }
        };
    }

    @Override
    public int getItemCount() {
        return chatcontent.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView user;
        TextView  name,email;
        LinearLayout chat_view;
        public ViewHolder(View itemView) {
            super(itemView);
            user  =  (ImageView)itemView.findViewById(R.id.chat_image);
            name  =  (TextView) itemView.findViewById(R.id.chat_user_name);
            email = (TextView) itemView.findViewById(R.id.chat_email);
            chat_view = (LinearLayout)itemView.findViewById(R.id.chat_view);
        }
    }
}
