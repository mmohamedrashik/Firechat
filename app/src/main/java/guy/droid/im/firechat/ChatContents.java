package guy.droid.im.firechat;

/**
 * Created by admin on 7/24/2017.
 */

public class ChatContents {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String imahe_url) {
        this.image_url = imahe_url;
    }



    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    String id;
    String name;
    String image_url;
    String email_id;
    String message;

}
